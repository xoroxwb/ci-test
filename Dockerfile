FROM alpine

RUN uname -m

COPY overlay /

ENTRYPOINT [ "/entrypoint" ]